En partant sur le principe du reconditionnement de mobilier composé de structure métallique, nous avons remarqué que la structure métallique des 30 chaises présentes dans la cafétéria avaient un certain potentiel.

Il fallait modifier la hauteur et l’angle du dossier pour être mieux adaptés à l’usage du repas. L’uniformisation des matériaux entre tous les mobiliers de la salle apporte aussi une harmonie à l’espace. Ce matériau en question est un panneau de bois (3 plis mélèze 19mm).

Nous avons recherché une forme ergonomique, avec la contrainte de la structure acier et de ce panneau de bois. La solution devait être réalisable simplement et reproductible, avec l’outillage de l’atelier monumental de l’école. Après plusieurs essais, une solution a été retenue et lancée en fabrication.